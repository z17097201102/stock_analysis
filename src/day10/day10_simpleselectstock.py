# 可以自己import我们平台支持的第三方python模块，比如pandas、numpy等。
# 为了不报错引入的模块
import logging as logger
from util.util import *
from util.finance import *
import util.scheduler as scheduler
# 机器学习使用
from sklearn.preprocessing import StandardScaler
from scipy.stats.mstats import winsorize
from sklearn.linear_model import LinearRegression, Ridge

# numpy ,panda
import pandas as pd
import numpy as np 


# 在这个方法中编写任何的初始化逻辑。context对象将会在你的算法策略的任何方法之间做传递。
def init(context):
    # 在context中保存全局变量
    # context.s1 = "000001.XSHE"
    # 实时打印日志
    # logger.info("RunInfo: {}".format(context.run_info))
    # 定义一个选股的范围
    context.hs300 = index_components("000300.XSHG")

    scheduler.run_monthly(get_data, tradingday=1)


def get_data(context, bar_dict):
    # 删掉两个条件
    # .filter(
    #         fundamentals.eod_derivative_indicator.pe_ratio > 50
    #         ).filter(
    #             fundamentals.eod_derivative_indicator.pe_ratio < 65
    #             )

    # 选股
    q = query(fundamentals.eod_derivative_indicator.pe_ratio,
              fundamentals.income_statement.revenue
              ).order_by(
        fundamentals.income_statement.revenue.desc()
    ).filter(
        fundamentals.stockcode.in_(context.hs300)
    ).limit(10)

    fund = get_fundamentals(q)

    # 行列内容以及索引一起进行转置
    # print(fund.T)
    context.stock_list = fund.T.index


# before_trading此函数会在每天策略交易开始前被调用，当天只会被调用一次
def before_trading(context):
    pass


# 你选择的证券的数据更新将会触发此段逻辑，例如日或分钟历史数据切片或者是实时数据切片更新
def handle_bar(context, bar_dict):
    # 开始编写你的主要的算法逻辑

    # bar_dict[order_book_id] 可以拿到某个证券的bar信息
    # context.portfolio 可以拿到现在的投资组合信息

    # 使用order_shares(id_or_ins, amount)方法进行落单

    # TODO: 开始编写你的算法吧！
    # order_shares(context.s1, 1000)

    # 在这里才能进行交易
    # 先判断仓位是否有股票，如果有，卖出（判断在不在新的股票池当中）
    if len(context.portfolio.positions.keys()) != 0:

        for stock in context.portfolio.positions.keys():

            # 如果旧的持有的股票不在新的股票池当中，卖出
            if stock not in context.stock_list:
                order_target_percent(stock, 0)

    # 买入最新的每日更新的股票池当中的股票
    # 等比例资金买入，投资组合总价值的百分比平分10份
    weight = 1.0 / len(context.stock_list)

    for stock in context.stock_list:
        order_target_percent(stock, weight)


# after_trading函数会在每天交易结束后被调用，当天只会被调用一次
def after_trading(context):
    pass
