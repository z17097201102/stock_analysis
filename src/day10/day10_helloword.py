# 可以自己import我们平台支持的第三方python模块，比如pandas、numpy等。
# 为了不报错引入的模块
import logging as logger
from util.util import *
from util.finance import *
import util.scheduler as scheduler
# 机器学习使用
from sklearn.preprocessing import StandardScaler
from scipy.stats.mstats import winsorize
from sklearn.linear_model import LinearRegression, Ridge

# numpy ,panda
import pandas as pd
import numpy as np 


# 在这个方法中编写任何的初始化逻辑。context对象将会在你的算法策略的任何方法之间做传递。
def init(context):
    # 在context中保存全局变量
    context.s1 = "000001.XSHE"
    context.stock = "0000007.XSHE"

    # 获取计算机通信行业的一些公司股票代码
    # context.stock_list = industry('C39')

    # 板块
    # context.sector_list = sector("energy")

    # 经常会调用指数成分股的接口
    # 获取沪深300的指数股票
    # 相当于股票池
    context.inex_list = index_components("000300.XSHG")

    # 定义按月运行的一个定时运行器
    # 每月只运行一次，指定第一个交易日
    # scheduler.run_monthly(get_data,tradingday=1)


def get_data(context, bar_dict):
    # 在这里按月去查询财务数据
    q = query(
        fundamentals.eod_derivative_indicator.pb_ratio
    ).filter(
        fundamentals.stockcode.in_(context.inex_list)
    )

    fund = get_fundamentals(q)

    logger.info(fund.T)


# before_trading此函数会在每天策略交易开始前被调用，当天只会被调用一次
def before_trading(context):
    # print(context.stock)
    # logger.info(context.stock)
    # print(context.inex_list)
    pass


# 你选择的证券的数据更新将会触发此段逻辑，例如日或分钟历史数据切片或者是实时数据切片更新
def handle_bar(context, bar_dict):
    # 开始编写你的主要的算法逻辑

    # bar_dict[order_book_id] 可以拿到某个证券的bar信息
    # context.portfolio 可以拿到现在的投资组合信息

    # 使用order_shares(id_or_ins, amount)方法进行落单

    # 进行交易
    # 每天的收盘价 假如第一天11.33 * 1000
    # order_shares(context.s1, 1000)

    order_target_percent(context.s1, 0.1)

    order_target_percent("000004.XSHE", 0.1)

    # 一旦买入交易的之后，我们投资组合会发生变化
    # 资金、仓位
    print(context.stock_account)
    print("-------------------------")

    print(context.portfolio.positions.keys())
    print(context.portfolio.positions[context.s1].quantity)

    print("-------------------------")

    print("投资组合的可用资金为", context.portfolio.cash)
    print("投资组合的市场价值为", context.portfolio.market_value)
    print("投资组合的总价值为", context.portfolio.total_value)


    # TODO: 开始编写你的算法吧！
    # order_shares(context.s1, 1000)
    # 第二个参数是总共获取的数据
    # # 5：从当前日期运行开始到之前的5天的行情数据
    # close = history_bars(context.s1, 5, '1d', 'close')
    # # 获取多个指标
    # history_1 = history_bars(context.s1, 5, '1d', ['close', 'open'])

    # # 吧频率改成'1m'
    # # close = history_bars(context.s1, 5, '1m', 'close')


    # logger.info(close)


    # 获取财务数据，默认是获取所有A股的股票财务数据

    # 创建一个查询语句
    # 增加filter
    # q = query(
    #     fundamentals.eod_derivative_indicator.pe_ratio,
    #     fundamentals.eod_derivative_indicator.pcf_ratio
    #     ).filter(
    #         fundamentals.eod_derivative_indicator.pe_ratio > 20,
    #         fundamentals.eod_derivative_indicator.pcf_ratio < 50
    #         ).order_by(
    #             fundamentals.eod_derivative_indicator.pe_ratio
    #             ).filter(
    #                 fundamentals.stockcode.in_(context.inex_list)
    #                 ).limit(10)

    # # 回测不需要日期，默认当天的数据
    # fund = get_fundamentals(q)

    # logger.info(fund.T)


# after_trading函数会在每天交易结束后被调用，当天只会被调用一次
def after_trading(context):
    pass













