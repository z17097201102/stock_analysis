def get_fundamentals(obj):
    pass


def query(*args):
    return {}
    pass


# order_shares - 指定股数交易（股票专用）
# order_lots - 指定手数交易（股票专用）
# order_value - 指定价值交易（股票专用） 每次出多少钱买股票
# order_percent - 一定比例下单（股票专用） 每次买多少比例
# order_target_value - 目标价值下单（股票专用） 持仓最终数量
# order_target_percent - 目标比例下单（股票专用）持仓最终占比

"""
order_shares - 指定股数交易（股票专用）
order_shares(id_or_ins, amount, style=MarketOrder())
落指定股数的买/卖单，最常见的落单方式之一。如有需要落单类型当做一个参量传入，如果忽略掉落单类型，那么默认是市价单（market order）。

参数

参数	类型	注释
id_or_ins	str 或 instrument 对象	order_book_id 或 symbol 或 instrument 对象，用户必须指定
amount	float-required	需要落单的股数。正数代表买入，负数代表卖出。将会根据一手 xx 股来向下调整到一手的倍数，比如中国 A 股就是调整成 100 股的倍数。
style	OrderType	订单类型，默认是市价单。目前支持的订单类型有：
style=MarketOrder()
style=LimitOrder(limit_price)
"""


def order_shares(id_or_ins, amount, style=None):
    pass


"""
order_lots - 指定手数交易（股票专用）
order_lots(id_or_ins, amount, style=OrderType)
指定手数发送买/卖单。如有需要落单类型当做一个参量传入，如果忽略掉落单类型，那么默认是市价单（market order）。

参数

参数	类型	注释
id_or_ins	str或 instrument 对象	order_book_id 或 symbol 或 instrument 对象，用户必须指定
amount	float	多少手的数目。正数表示买入，负数表示卖出，用户必须指定
style	OrderType	订单类型，默认是市价单。目前支持的订单类型有：
style=MarketOrder
style=LimitOrder(limit_price)
"""


def order_lots(id_or_ins, amount, style=None):
    pass


"""
order_value - 指定价值交易（股票专用）
order_value(id_or_ins, cash_amount, style=OrderType)
使用想要花费的金钱买入/卖出股票，而不是买入/卖出想要的股数，正数代表买入，负数代表卖出(暂不支持卖空)。股票的股数总是会被调整成对应的 100 的倍数（在 A 中国 A 股市场 1 手是 100 股）。当您提交一个卖单时，该方法代表的意义是您希望通过卖出该股票套现的金额。如果金额超出了您所持有股票的价值，那么您将卖出所有股票。需要注意，如果资金不足，该 API 将不会创建发送订单。

参数

参数	类型	注释
id_or_ins	str或 instrument 对象	order_book_id 或 symbol 或 instrument 对象，用户必须指定
cash_amount	float	需要花费现金购买/卖出证券的数目。正数代表买入，负数代表卖出，用户必须指定
style	OrderType	订单类型，默认是市价单。目前支持的订单类型有：
style=MarketOrder()
style=LimitOrder(limit_price)
"""


def order_value(id_or_ins, cash_amount, style=None):
    pass


"""
order_percent - 一定比例下单（股票专用）
order_percent(id_or_ins, percent, style=OrderType)
发送一个等于目前投资组合价值（市场价值和目前现金的总和）一定百分比的买/卖单，正数代表买，负数代表卖(暂不支持卖空)。股票的股数总是会被调整成对应的一手的股票数的倍数（1 手是 100 股）。百分比是一个小数，并且小于 1（<100%），0.5 表示的是 50%.需要注意，百分比不能为 1，因为下单时手续费将会计入下单金额中。当买入股票所需金额加上手续费大于资金时，该 API 将不会创建发送订单。

参数

参数	类型	注释
id_or_ins	str或 instrument 对象	order_book_id 或 symbol 或 instrument object，用户必须指定
percent	float	占有现有的投资组合价值的百分比。正数表示买入，负数表示卖出。用户必须指定
style	OrderType	订单类型，默认是市价单。目前支持的订单类型有：
style=MarketOrder()
style=LimitOrder(limit_price)
"""


def order_percent(id_or_ins, percent, style=None):
    pass


"""
order_target_value - 目标价值下单（股票专用）
order_target_value(id_or_ins, cash_amount, style=OrderType)
买入/卖出并且自动调整该证券的仓位到一个目标价值(暂不支持卖空)。如果还没有任何该证券的仓位，那么会买入全部目标价值的证券。如果已经有了该证券的仓位，则会买入/卖出调整该证券的现在仓位和目标仓位的价值差值的数目的证券。需要注意，如果资金不足，该 API 将不会创建发送订单。

参数

参数	类型	注释
id_or_ins	str或 instrument 对象	order_book_id 或 symbol 或 instrument object，用户必须指定
cash_amount	float-required	最终的该证券的仓位目标价值
style	OrderType	订单类型，默认是市价单。目前支持的订单类型有：
style=MarketOrder()
style=LimitOrder(limit_price)
"""


def order_target_value(id_or_ins, cash_amount, style=None):
    pass


"""
order_target_percent - 目标比例下单（股票专用）
order_target_percent(id_or_ins, percent, style=OrderType)
买入/卖出证券以自动调整该证券的仓位到占有一个指定的投资组合的目标百分比(暂不支持卖空)。

如果投资组合中没有任何该证券的仓位，那么会买入等于现在投资组合总价值的目标百分比的数目的证券。
如果投资组合中已经拥有该证券的仓位，那么会买入/卖出目标百分比和现有百分比的差额数目的证券，最终调整该证券的仓位占据投资组合的比例至目标百分比。
其实我们需要计算一个 position_to_adjust (即应该调整的仓位)

position_to_adjust = target_position - current_position
投资组合价值等于所有已有仓位的价值和剩余现金的总和。买/卖单会被下舍入一手股数（A 股是 100 的倍数）的倍数。目标百分比应该是一个小数，并且最大值应该<=1，比如 0.5 表示 50%。

如果position_to_adjust 计算之后是正的，那么会买入该证券，否则会卖出该证券。
需要注意，如果资金不足，该 API 将不会创建发送订单。

参数

参数	类型	注释
id_or_ins	str或 instrument 对象	order_book_id 或 symbol 或 instrument object，用户必须指定
percent	float-required	仓位最终所占投资组合总价值的目标百分比。
style	OrderType	订单类型，默认是市价单。目前支持的订单类型有：
style=MarketOrder()
style=LimitOrder(limit_price)
"""


def order_target_percent(id_or_ins, cash_amount, style=None):
    pass


# class fundamentals(object):
#     def __init__(self):
#         pass
# 
#     pass


"""
fundamentals对象太大了,所以我使用方法来替换,不报红错误就行了
"""


def fundamentals():
    return {}
    pass


__all__ = [get_fundamentals, query, order_shares, order_lots, order_value,
           order_percent, order_target_value, order_target_percent, fundamentals]
