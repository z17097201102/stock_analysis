def run_daily(function):
    """
    scheduler.run_daily - 每天运行
    scheduler.run_daily(function)
    每日运行一次指定的函数，只能在 init 内使用。

    注意，schedule 一定在其对应时间点的 handle_bar 之前执行，如果定时运行函数运行时间较长，则中间的 handle_bar 事件将会被略过。

    参数

    参数	类型	注释
    function	function	使传入的function每日运行。注意，function 函数一定要包含（并且只能包含）context, bar_dict 两个输入参数
    """
    pass


def run_weekly(function, weekday=1, tradingday=1):
    """ 
    scheduler.run_weekly - 每周运行
    scheduler.run_weekly(function, weekday=x, tradingday=t)
    每周运行一次指定的函数，只能在 init 内使用。
    
    注意：
    
    tradingday中的负数表示倒数。
    
    tradingday表示交易日。如某周只有四个交易日，则此周的tradingday=4与tradingday=-1表示同一天。
    
    weekday和tradingday不能同时使用。
    
    参数
    
    参数	类型	注释
    function	function	使传入的function每日交易开始前运行。注意，function 函数一定要包含（并且只能包含）context, bar_dict 两个输入参数
    weekday	int	1~5 分别代表周一至周五，用户必须指定
    tradingday	int	范围为[-5,1],[1,5] 例如，1 代表每周第一个交易日，-1 代表每周倒数第一个交易日，用户可以不填写
    """
    pass


def run_monthly(function, tradingday=1):
    """ 
     scheduler.run_monthly - 每月运行
    scheduler.run_monthly(function,tradingday=t)
    每月运行一次指定的函数，只能在 init 内使用。
    
    注意:
    
    tradingday的负数表示倒数。
    
    tradingday表示交易日，如某月只有三个交易日，则此月的 tradingday=3 与 tradingday=-1 表示同一。
    
    参数
    
    参数	类型	注释
    function	function	使传入的function每日交易开始前运行。注意，function 函数一定要包含（并且只能包含）context, bar_dict 两个输入参数
    tradingday	int	范围为[-23,1], [1,23] ，例如，1 代表每月第一个交易日，-1 代表每月倒数第一个交易日，用户必须指定

     """
    pass


__all__ = [run_daily, run_weekly, run_monthly]
