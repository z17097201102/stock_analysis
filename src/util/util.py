def all_instruments(type=None, market='cn', date=None):
    """
    type	str	需要查询合约类型，例如：type='CS'代表股票。默认是所有类型
    market	str	默认是中国内地市场('cn') 。可选'cn' - 中国内地市场；'hk' - 香港市场
    date	str, datetime.date, datetime.datetime, pandasTimestamp	指定日期，筛选指定日期可交易的合约
    """
    pass


def index_components(order_book_id, date=None, start_date=None, end_date=None, market='cn'):
    """
    index_components(order_book_id, date=None, start_date, end_date, market='cn')
    参数	类型	说明
    order_book_id	str	指数代码，传入 order_book_id，例如'000001.XSHG'。
    date	str, datetime.date, datetime.datetime, pandas Timestamp	查询日期，默认为最新记录日期
    start_date	str, datetime.date, datetime.datetime, pandas Timestamp	指定开始日期，不能和 date 同时指定
    end_date	str, datetime.date, datetime.datetime, pandas Timestamp	指定结束日期, 需和 start_date 同时指定并且应当不小于开始日期
    market	str	默认是中国市场('cn')，目前仅支持中国市场

    """
    pass


def history_bars(order_book_id, bar_count, frequency, fields=None, skip_suspended=True, include_now=False):
    """
    参数	类型	注释
    order_book_id	str	合约代码，必填项
    bar_count	int	获取的历史数据数量，必填项
    frequency	str	获取数据什么样的频率进行。'1d'或'1m'分别表示每日和每分钟，必填项。您可以指定不同的分钟频率，例如'5m'代表 5 分钟线
    fields	str OR str list	返回数据字段。必填项。见下方列表
    skip_suspended	bool	是否跳过停牌，默认 True，跳过停牌
    include_now	bool	是否包括不完整的 bar 数据。默认为 False，不包括。举例来说，在 09:39 的时候获取上一个 5 分钟线，默认将获取到 09:31~09:35 合成的 5 分钟线。如果设置为 True，则将获取到 09:36~09:39 之间合成的"不完整"5 分钟线
    adjust_type	str	复权方式，默认为pre。
    不复权 - none，
    动态前复权 - pre，后复权 - post
    """
    """
    fields	字段名
    open	开盘价
    high	最高价
    low	最低价
    close	收盘价
    volume	成交量
    total_turnover	成交额
    datetime	int 类型时间戳
    open_interest	持仓量（期货专用）
    settlement	结算价（期货日线专用）
    prev_settlement	结算价（期货日线专用）
    """
    pass


def is_st_stock(order_book_ids, start_date, end_date):
    """ 
    判断一只或多只股票在一段时间（包含起止日期）内是否为 ST 股。

    ST 股包括如下:
    
    S*ST-公司经营连续三年亏损，退市预警+还没有完成股改;
    *ST-公司经营连续三年亏损，退市预警;
    ST-公司经营连续二年亏损，特别处理;
    SST-公司经营连续二年亏损，特别处理+还没有完成股改;
 
    参数	类型	说明
    order_book_ids	str or str list	合约代码，可传入 order_book_id, order_book_id list
    start_date	str, datetime.date, datetime.datetime, pandasTimestamp	开始日期
    end_date	str, datetime.date, datetime.datetime, pandasTimestamp	结束日期
    """
    pass


def is_suspended(order_book_ids, start_date=None, end_date=None, market='cn'):
    """
    is_suspended - 判断股票是否全天停牌
    is_suspended(order_book_ids, start_date=None, end_date=None, market='cn')
    判断某只股票在一段时间（包含起止日期）是否全天停牌。

    #参数
    参数	类型	说明
    order_book_ids	str or list	合约代码。传入单只或多支股票的 order_book_id
    start_date	str, datetime.date, datetime.datetime, pandasTimestamp	开始日期，默认为股票上市日期
    end_date	str, datetime.date, datetime.datetime, pandasTimestamp	结束日期，默认为当前日期，如果股票已经退市，则为退市日期
    market	str	默认是中国内地市场('cn') 。可选'cn' - 中国内地市场；'hk' - 香港市场
    """
    pass




def instruments(id_or_symbols):
    """
    参数	类型	说明
    id_or_symbols	str OR str list	合约代码或合约代码列表，可传入 order_book_id, order_book_id list。
    中国市场的 order_book_id 通常类似'000001.XSHE'。
    需要注意，国内股票、ETF、指数合约代码分别应当以'.XSHG'或'.XSHE'结尾，前者代表上证，后者代表深证。
    比如查询平安银行这个股票合约，则键入'000001.XSHE'，前面的数字部分为交易所内这个股票的合约代码，后半部分为对应的交易所代码。
    期货则无此要求
   
    """

    
    """
    
        return:一个Instrument 对象，或一个 instrument list。
    """
    """
    Instrument 对象
    股票，ETF，指数 Instrument 对象
    参数	类型	说明
    order_book_id	str	证券代码，证券的独特的标识符。应以'.XSHG'或'.XSHE'结尾，前者代表上证，后者代表深证
    symbol	str	证券的简称，例如'平安银行'
    abbrev_symbol	str	证券的名称缩写，在中国 A 股就是股票的拼音缩写。例如：'PAYH'就是平安银行股票的证券名缩写
    round_lot	int	一手对应多少股，中国 A 股一手是 100 股
    sector_code	str	板块缩写代码，全球通用标准定义
    sector_code_name	str	以当地语言为标准的板块代码名
    industry_code	str	国民经济行业分类代码，具体可参考下方"Industry 列表"
    industry_name	str	国民经济行业分类名称
    listed_date	str	该证券上市日期
    de_listed_date	str	退市日期
    type	str	合约类型，目前支持的类型有: 'CS', 'INDX', 'LOF', 'ETF', 'Future'
    concept_names	str	概念股分类，例如：'铁路基建'，'基金重仓'等
    exchange	str	交易所，'XSHE' - 深交所, 'XSHG' - 上交所
    board_type	str	板块类别，'MainBoard' - 主板,'GEM' - 创业板,'SME' - 中小板
    status	str	合约状态。'Active' - 正常上市, 'Delisted' - 终止上市, 'TemporarySuspended' - 暂停上市, 'PreIPO' - 发行配售期间, 'FailIPO' - 发行失败
    special_type	str	特别处理状态。'Normal' - 正常上市, 'ST' - ST 处理, 'StarST' - *ST 代表该股票正在接受退市警告, 'PT' - 代表该股票连续 3 年收入为负，将被暂停交易, 'Other' - 其他
    trading_hours	str	合约交易时间
    """

    """
    
         获取合约已上市天数： instruments('000001.XSHE').days_from_listed()
    """
    pass

__all__ = [all_instruments, index_components, history_bars, is_st_stock, is_suspended, instruments]
if __name__ == '__main__':
    pass
