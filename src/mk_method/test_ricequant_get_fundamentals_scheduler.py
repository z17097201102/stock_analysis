# 可以自己import我们平台支持的第三方python模块，比如pandas、numpy等。
# 为了不报错引入的模块
import logging as logger
from util.util import *
from util.finance import *
import util.scheduler as scheduler
# 机器学习使用
from sklearn.preprocessing import StandardScaler
from scipy.stats.mstats import winsorize
from sklearn.linear_model import LinearRegression, Ridge

# numpy ,panda
import pandas as pd
import numpy as np 



# 在这个方法中编写任何的初始化逻辑。context对象将会在你的算法策略的任何方法之间做传递。
from util.util import *
import logging as logger
def init(context):
    # 在context中保存全局变量
    context.s1 = "003032.XSHE"
    # 实时打印日志
    logger.info("RunInfo: {}".format(context.run_info))

    # 经常会调用指数成分股的接口
    # 获取沪深300的指数股票
    # 相当于股票池
    context.hs300 = index_components("000300.XSHG")
    # 定义按月运行的一个定时运行器
    # 每月只运行一次，指定第一个交易日
    # scheduler.run_daily - 每天运行
    # scheduler.run_weekly - 每周运行,是每周第一个交易日运行
    # scheduler.run_monthly - 每月运行,是每月第一个交易日运行
    # 使传入的function每日运行。注意，function函数一定要包含（并且只能包含）context, bar_dict两个输入参数
    # tradingday为第几个交易日,
    # 在before_trading 和handler_bar中间运行函数
    scheduler.run_monthly(get_data,tradingday=1)
def get_data(context, bar_dict):
    # 市盈率 pe_ratio 
    # 市现率 pcf_ratio 
    # fundamentals获取所有股票的财务数据
    q = query(fundamentals.eod_derivative_indicator.pe_ratio,
              fundamentals.eod_derivative_indicator.pcf_ratio
              ).filter(
        fundamentals.eod_derivative_indicator.pe_ratio > 20,
        ).order_by(
        fundamentals.eod_derivative_indicator.pe_ratio
    ).filter(
        fundamentals.stockcode.in_(context.hs300)
    ).limit(20)

    # 获取财务数据，默认获取的是dataframe，entry_date在回测当中不去要提供
    # get_fundamentals is deprecated, use get_factor instead
    fund = get_fundamentals(q)
    logger.info(fund.T)
    

# before_trading此函数会在每天策略交易开始前被调用，当天只会被调用一次
def before_trading(context):
    pass


# 你选择的证券的数据更新将会触发此段逻辑，例如日或分钟历史数据切片或者是实时数据切片更新
# bar_dict 获取当天数据
def handle_bar(context, bar_dict):
    # # 获取财务数据，默认是获取所有A股的股票财务数据
    # 
    # 增加条件过滤掉不符合的股票代码
    # 默认直接获取A股是所有的股票这个指标数据
    # order_by默认是升序
    # limit:选择固定数量的股票，获取20个股票交易
    # q = query(fundamentals.eod_derivative_indicator.pe_ratio,
    #           fundamentals.eod_derivative_indicator.pcf_ratio
    # ).filter(
    # fundamentals.eod_derivative_indicator.pe_ratio > 20,
    # fundamentals.eod_derivative_indicator.pcf_ratio > 15
    # ).order_by(
    #     fundamentals.eod_derivative_indicator.pe_ratio
    # ).limit(20)
    # 
    # 想要从沪深300指数的一些股票去进行筛选
    # 通过fundamentals.stockcode去限定股票池
    # 
    # 
    # 开始编写你的主要的算法逻辑
    # 
    # bar_dict[order_book_id] 可以拿到某个证券的bar信息
    # context.portfolio 可以拿到现在的投资组合信息
    # 
    # 使用order_shares(id_or_ins, amount)方法进行落单
    # 
    # TODO: 开始编写你的算法吧！
    # order_shares(context.s1, 1000)
    pass


# after_trading函数会在每天交易结束后被调用，当天只会被调用一次
def after_trading(context):
    pass
