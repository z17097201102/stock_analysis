# 可以自己import我们平台支持的第三方python模块，比如pandas、numpy等。
# 为了不报错引入的模块
import logging as logger
from util.util import *
from util.finance import *
import util.scheduler as scheduler
# 机器学习使用
from sklearn.preprocessing import StandardScaler
from scipy.stats.mstats import winsorize
from sklearn.linear_model import LinearRegression, Ridge

# numpy ,panda
import pandas as pd
import numpy as np 




# 在这个方法中编写任何的初始化逻辑。context对象将会在你的算法策略的任何方法之间做传递。
def init(context):
    # 在context中保存全局变量
    context.s1 = "003032.XSHE"
    # 实时打印日志
    logger.info("RunInfo: {}".format(context.run_info))


# before_trading此函数会在每天策略交易开始前被调用，当天只会被调用一次
def before_trading(context):
    pass


# 你选择的证券的数据更新将会触发此段逻辑，例如日或分钟历史数据切片或者是实时数据切片更新
# bar_dict 获取当天数据
def handle_bar(context, bar_dict):
    if bar_dict[context.s1].isnan:
        # 没有行情 suspended True
        # suspended = bar_dict[context.s1].suspended
        # logger.info("suspended %s" % suspended) 
        return
    close = bar_dict[context.s1].close
    # logger.info("close %.2f" % close)
    open = bar_dict[context.s1].open
    # logger.info("open %.2f" % open)
    high = bar_dict[context.s1].high
    # logger.info("high %.2f" % high)
    low = bar_dict[context.s1].low
    # logger.info("low %.2f" % low)
    suspended = bar_dict[context.s1].suspended
    logger.info(  suspended)
    limit_up = bar_dict[context.s1].limit_up
    # logger.info("limit_up %.2f" % limit_up)
    limit_down = bar_dict[context.s1].limit_down
    # logger.info("limit_down %.2f" % limit_down)
    prev_close = bar_dict[context.s1].prev_close
    if pd.isnull(prev_close):
        # 昨天没有行情
        logger.info("prev_close %.2f" % prev_close)
        logger.info("上市第一天,上涨率 %.2f" % (limit_up / open))
        pass
    else:
        logger.info("prev_close %.2f" % prev_close)
        logger.info("上涨率 %.2f" % (close / prev_close))
    # 开始编写你的主要的算法逻辑

    # bar_dict[order_book_id] 可以拿到某个证券的bar信息
    # context.portfolio 可以拿到现在的投资组合信息

    # 使用order_shares(id_or_ins, amount)方法进行落单

    # TODO: 开始编写你的算法吧！
    # order_shares(context.s1, 1000)


# after_trading函数会在每天交易结束后被调用，当天只会被调用一次
def after_trading(context):
    pass
