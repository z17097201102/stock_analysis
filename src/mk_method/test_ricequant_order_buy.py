# 可以自己import我们平台支持的第三方python模块，比如pandas、numpy等。
# 为了不报错引入的模块
import logging as logger
from util.util import *
from util.finance import *
import util.scheduler as scheduler
# 机器学习使用
from sklearn.preprocessing import StandardScaler
from scipy.stats.mstats import winsorize
from sklearn.linear_model import LinearRegression, Ridge

# numpy ,panda
import pandas as pd
import numpy as np 




# 在这个方法中编写任何的初始化逻辑。context对象将会在你的算法策略的任何方法之间做传递。
def init(context):
    # 在context中保存全局变量
    context.s1 = "003032.XSHE"
    # context.s1 = "600526.XSHG"


# before_trading此函数会在每天策略交易开始前被调用，当天只会被调用一次
def before_trading(context):
    pass


# 你选择的证券的数据更新将会触发此段逻辑，例如日或分钟历史数据切片或者是实时数据切片更新
# bar_dict 获取当天数据
def handle_bar(context, bar_dict):
    # 1.1 交易方法
    # order_shares - 指定股数交易（股票专用）
    # order_lots - 指定手数交易（股票专用）
    # order_value - 指定价值交易（股票专用） 每次出多少钱买股票
    # order_percent - 一定比例下单（股票专用） 每次买多少比例
    # order_target_value - 目标价值下单（股票专用） 持仓最终数量
    # order_target_percent - 目标比例下单（股票专用）持仓最终占比

    # LimitOrder(limit_price),需要使用特定价钱下单

    # 通过判断最低点是否满足要求了,
    # 如果满足要求了就直接买了,这里不支持低于收盘价买入,限价单 style=LimitOrder(limit_price)名存实亡

    # order_percent(context.s1, 0) 
    # order_percent(context.s1, 0.5)
    # 开始编写你的交易吧！
    # 卖出 可以用收盘价
    # 我一般卖出用市价单,还有就是要加入滑点
    # order_percent(context.s1, 0, style=MarketOrder())
    # 买入 style=LimitOrder(limit_price)
    # 我一般买入用限价单,还有就是要加入滑点,
    # 怎么看限价单是否可以买到呢
    # order_percent(context.s1, 0.4, style=LimitOrder(8.1))
    # order_percent(context.s1,0.3)
    # order_percent(context.s1,0.3)

    # 1.2下单不成功
    # portfolio内可用资金不足
    # 下单数量不足一手（股票为100股）
    # 下单价格超过当日涨跌停板限制
    # 当前可卖（可平）仓位不足
    # 股票当日停牌
    # 合约已经退市（到期）或尚未上市

    # 1.3 何为市价单和限价单
    # 我一般买入用限价单,还有就是要加入滑点
    # order_percent(context.s1, 0.4)

    # 2、投资组合
    # 一旦买入交易的之后，我们投资组合会发生变化
    # 资金、仓位
    logger.info(" 当前时间 %s" % context.now)
    logger.info(" 股票资金账户信息 %s" % context.stock_account)
    # order_target_percent(context.s1, 0.5)
    order_value(context.s1, 10000, price=22.23)
    # order_value(context.s1)
    # order_percent(context.s1, 0.5)
    #   portfolio - 投资组合信息

    logger.info("投资组合的可用资金为 %.2f" % context.portfolio.cash)
    logger.info("投资组合的市场价值为 %.2f" % context.portfolio.market_value)
    logger.info("投资组合的总价值为 %.2f" % context.portfolio.total_value)

    #  positions	dict	一个包含所有仓位的字典，
    #  以order_book_id作为键，position对象作为值，
    #  关于position的更多的信息可以在下面的部分找到。
    logger.info(context.portfolio.positions.keys())
    logger.info(context.portfolio.positions[context.s1].quantity)
    pass


# after_trading函数会在每天交易结束后被调用，当天只会被调用一次
def after_trading(context):
    pass
