# 可以自己import我们平台支持的第三方python模块，比如pandas、numpy等。
# 为了不报错引入的模块
import logging as logger
from util.util import *
from util.finance import *
import util.scheduler as scheduler
# 机器学习使用
from sklearn.preprocessing import StandardScaler
from scipy.stats.mstats import winsorize
from sklearn.linear_model import LinearRegression, Ridge

# numpy ,panda
import pandas as pd
import numpy as np 




# 4、中位数绝对偏差去极值
def median(factor):
    """3倍中位数去极值
    """
    # 求出因子值的中位数
    med = np.median(factor)
    # 求出因子值与中位数的差值，进行绝对值
    mad = np.median(abs(factor - med))

    # 定义几倍的中位数上下限
    high = med + (3 * 1.4826) * mad
    low = med - (3 * 1.4826) * mad

    # 替换上下限以外的值
    factor = np.where(factor > high, high, factor)
    factor = np.where(factor < low, low, factor)
    return factor


def stand(factor):
    """
    自实现标准化
    """
    mean = factor.mean()
    std = factor.std()
    return (factor - mean) / std


#  建立单因子的回测框架
# 回测区间 2017-01-01~2018-01-01
# 调仓频率：一个月，买卖判断


# 在这个方法中编写任何的初始化逻辑。context对象将会在你的算法策略的任何方法之间做传递。
def init(context):
    context.hs300 = index_components("000300.XSHG")
    # 在context中保存全局变量
    context.quantile = 5
    scheduler.run_monthly(select_stock, tradingday=1)


def select_stock(context, bar_dict):
    # 从市值中选择值小的股票
    q = query(fundamentals.financial_indicator.earnings_per_share).filter(
        fundamentals.stockcode.in_(context.hs300)
    ).limit(20)
    factor = get_fundamentals(q).T
    # 2、定义函数去处理数据
    logger.info(factor.head())

    # 按照百分位选出市净率小的一些股票交易
    # 去极值,标准化处理,
    factor['earnings_per_share'] = median(factor['earnings_per_share'])
    factor['earnings_per_share'] = stand(factor['earnings_per_share'])

    # 求分位数是对于一列数据，dataframe求不了
    data = factor.iloc[:, 0]

    # 按照分位去分成5组，分别进行回测
    # [0.2, 0.4, 0.6, 0.8]
    if context.quantile == 1:
        data = data[data <= data.quantile(0.2)]
        pass
    elif context.quantile == 2:
        data = data[data > data.quantile(0.2) & data <= data.quantile(0.4)]
        pass
    elif context.quantile == 3:
        data = data[data > data.quantile(0.4) & data <= data.quantile(0.6)]
        pass
    elif context.quantile == 4:
        data = data[data > data.quantile(0.6) & data <= data.quantile(0.8)]
        pass
    elif context.quantile == 5:
        data = data[data > data.quantile(0.8)]
        pass

    # 建立回测的股票池
    context.stock_list = data.index

    # 行列内容以及索引一起进行转置

    logger.info(len(context.stock_list))

    # 调仓频率：一个月，买卖判断
    # 进行每日调仓（多因子选股调仓周期频率会小一些）
    holding_list = context.portfolio.positions.keys()
    will_buy_list = context.stock_list.tolist()
    sell_list = set(holding_list) - set(will_buy_list)
    buy_list = set(will_buy_list) - set(holding_list)

    # 在这里才能进行卖出
    for stock in sell_list:
        # 如果旧的持有的股票不在新的股票池当中，卖出
        order_target_percent(stock, 0)

    # 等比例资金买入，投资组合总价值的百分比平分10份
    weight = 1.0 / len(context.stock_list)

    # 买入最新的每日更新的股票池当中的股票
    for stock in buy_list:
        order_target_percent(stock, weight)
    # logger.info(context.portfolio.positions.keys())


# before_trading此函数会在每天策略交易开始前被调用，当天只会被调用一次
def before_trading(context):
    pass


# 你选择的证券的数据更新将会触发此段逻辑，例如日或分钟历史数据切片或者是实时数据切片更新
def handle_bar(context, bar_dict):
    pass


# after_trading函数会在每天交易结束后被调用，当天只会被调用一次
def after_trading(context):
    pass
