# 可以自己import我们平台支持的第三方python模块，比如pandas、numpy等。
# 为了不报错引入的模块
import logging as logger
from util.util import *
from util.finance import *
import util.scheduler as scheduler
# 机器学习使用
from sklearn.preprocessing import StandardScaler
from scipy.stats.mstats import winsorize
from sklearn.linear_model import LinearRegression, Ridge

# numpy ,panda
import pandas as pd
import numpy as np 




# 在这个方法中编写任何的初始化逻辑。context对象将会在你的算法策略的任何方法之间做传递。
def init(context):
    scheduler.run_daily(get_data)
    context.hs300 = index_components("000300.XSHG")


def get_data(context, bar_dict):
    # 从市值中选择值小的股票
    q = query(fundamentals.eod_derivative_indicator.market_cap
              ).order_by(
        fundamentals.eod_derivative_indicator.market_cap.asc()
    ).filter(
        fundamentals.stockcode.in_(context.hs300)
    ).limit(10)

    fund = get_fundamentals(q)

    # 行列内容以及索引一起进行转置
    # print(fund.T)
    context.stock_list = fund.T.index


# before_trading此函数会在每天策略交易开始前被调用，当天只会被调用一次
def before_trading(context):
    pass


# 你选择的证券的数据更新将会触发此段逻辑，例如日或分钟历史数据切片或者是实时数据切片更新
def handle_bar(context, bar_dict):
    # 进行每日调仓（多因子选股调仓周期频率会小一些）
    holding_list = context.portfolio.positions.keys()
    will_buy_list = context.stock_list.tolist()
    sell_list = set(holding_list) - set(will_buy_list)
    buy_list = set(will_buy_list) - set(holding_list)

    # 在这里才能进行卖出
    for stock in sell_list:
        # 如果旧的持有的股票不在新的股票池当中，卖出
        order_target_percent(stock, 0)

    # 等比例资金买入，投资组合总价值的百分比平分10份
    weight = 0.1

    # 买入最新的每日更新的股票池当中的股票
    for stock in buy_list:
        order_target_percent(stock, weight)
    logger.info(context.portfolio.positions.keys())


# after_trading函数会在每天交易结束后被调用，当天只会被调用一次
def after_trading(context):
    pass
