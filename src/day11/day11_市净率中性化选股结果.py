# 可以自己import我们平台支持的第三方python模块，比如pandas、numpy等。
# 为了不报错引入的模块
import logging as logger
from util.util import *
from util.finance import *
import util.scheduler as scheduler
# 机器学习使用
from sklearn.preprocessing import StandardScaler
from scipy.stats.mstats import winsorize
from sklearn.linear_model import LinearRegression, Ridge

# numpy ,panda
import pandas as pd
import numpy as np 



# 在这个方法中编写任何的初始化逻辑。context对象将会在你的算法策略的任何方法之间做传递。
def init(context):
    # 在context中保存全局变量
    # context.s1 = "000001.XSHE"
    # # 实时打印日志
    # logger.info("RunInfo: {}".format(context.run_info))
    scheduler.run_monthly(get_data, tradingday=1)


def get_data(context, bar_dict):
    # 查询两个因子的数据结果
    fund = get_fundamentals(
        query(
            fundamentals.eod_derivative_indicator.pb_ratio,
            fundamentals.eod_derivative_indicator.market_cap
        ))

    # 查看fund格式
    # logger.info(fund)
    context.fund = fund.T

    # 进行因子数据的处理，去极值、标准化、市值中心化
    treat_data(context)

    # 利用市净率进行选股（市净率小的股票表现好）
    context.stock_list = context.fund['pb_ratio'][
        context.fund['pb_ratio'] <= context.fund['pb_ratio'].quantile(0.2)].index


def treat_data(context):
    """
    市净率因子数据的处理逻辑
    """
    # 对市净率去极值标准化
    context.fund['pb_ratio'] = mad(context.fund['pb_ratio'])
    context.fund['pb_ratio'] = stand(context.fund['pb_ratio'])

    # 选股的处理，对市净率进行市值中性化
    # 特征值：市值
    # 目标值：市净率因子
    x = context.fund['market_cap'].reshape(-1, 1)
    y = context.fund['pb_ratio']

    # 建立线性回归，中性化处理
    lr = LinearRegression()
    lr.fit(x, y)

    y_predict = lr.predict(x)

    context.fund['pb_ratio'] = y - y_predict


# before_trading此函数会在每天策略交易开始前被调用，当天只会被调用一次
def before_trading(context):
    pass


# 你选择的证券的数据更新将会触发此段逻辑，例如日或分钟历史数据切片或者是实时数据切片更新
def handle_bar(context, bar_dict):
    # 开始编写你的主要的算法逻辑

    # bar_dict[order_book_id] 可以拿到某个证券的bar信息
    # context.portfolio 可以拿到现在的投资组合信息

    # 使用order_shares(id_or_ins, amount)方法进行落单

    # TODO: 开始编写你的算法吧！
    # order_shares(context.s1, 1000)
    # 卖出
    for stock in context.portfolio.positions.keys():

        if stock not in context.stock_list:
            order_target_percent(stock, 0)

    weight = 1.0 / len(context.stock_list)

    # 买入
    for stock in context.stock_list:
        order_target_percent(stock, weight)


def mad(factor):
    """
    实现3倍中位数绝对偏差去极值
    """
    # 1、找出因子的中位数 median
    me = np.median(factor)

    # 2、得到每个因子值与中位数的绝对偏差值 |x – median|
    # 3、得到绝对偏差值的中位数， MAD，median(|x – median|)
    # np.median(abs(factor - me))就是MAD
    mad = np.median(abs(factor - me))

    # 4、计算MAD_e = 1.4826*MAD，然后确定参数 n，做出调整
    # 求出3倍中位数上下限制
    up = me + (3 * 1.4826 * mad)
    down = me - (3 * 1.4826 * mad)

    # 利用3倍中位数的值去极值
    factor = np.where(factor > up, up, factor)
    factor = np.where(factor < down, down, factor)
    return factor


def stand(factor):
    """自实现标准化
    """
    mean = factor.mean()
    std = factor.std()

    return (factor - mean) / std


# after_trading函数会在每天交易结束后被调用，当天只会被调用一次
def after_trading(context):
    pass
