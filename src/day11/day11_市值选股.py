# 可以自己import我们平台支持的第三方python模块，比如pandas、numpy等。
# 为了不报错引入的模块
import logging as logger
from util.util import *
from util.finance import *
import util.scheduler as scheduler
# 机器学习使用
from sklearn.preprocessing import StandardScaler
from scipy.stats.mstats import winsorize
from sklearn.linear_model import LinearRegression, Ridge

# numpy ,panda
import pandas as pd
import numpy as np 


# 在这个方法中编写任何的初始化逻辑。context对象将会在你的算法策略的任何方法之间做传递。
def init(context):
    # # 在context中保存全局变量
    # context.s1 = "000001.XSHE"
    # # 实时打印日志
    # logger.info("RunInfo: {}".format(context.run_info))
    context.hs300 = index_components("000300.XSHG")


# before_trading此函数会在每天策略交易开始前被调用，当天只会被调用一次
def before_trading(context):
    # 获取过滤的股票
    q = query(
        fundamentals.eod_derivative_indicator.market_cap
    ).order_by(
        fundamentals.eod_derivative_indicator.market_cap
    ).filter(
        fundamentals.stockcode.in_(context.hs300)
    ).limit(20)

    fund = get_fundamentals(q)

    # 获得10只股票的名字
    context.stock_list = fund.T.index


# 你选择的证券的数据更新将会触发此段逻辑，例如日或分钟历史数据切片或者是实时数据切片更新
def handle_bar(context, bar_dict):
    # 开始编写你的主要的算法逻辑

    # bar_dict[order_book_id] 可以拿到某个证券的bar信息
    # context.portfolio 可以拿到现在的投资组合信息

    # 使用order_shares(id_or_ins, amount)方法进行落单

    # TODO: 开始编写你的算法吧！
    # order_shares(context.s1, 1000)

    # 卖出
    # 去positions里面获取仓位
    for stock in context.portfolio.positions.keys():

        if stock not in context.stock_list:
            order_target_percent(stock, 0)

    # 买入
    for stock in context.stock_list:
        order_target_percent(stock, 1.0 / 20)


# after_trading函数会在每天交易结束后被调用，当天只会被调用一次
def after_trading(context):
    pass
